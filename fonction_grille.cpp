/*fonction_grille.cpp*/
#include "fonction_grille.h"
#include <iostream>
using namespace std;

void affiche_grille(Jeu *jeu) {
    cout << "     A   B   C   D   E   F   G   H " << endl;
    cout << "   +---+---+---+---+---+---+---+---+" << endl;
    for(int ligne=0; ligne<8; ligne++) {
        cout << " " << ligne+1 << " | ";
        for(int col=0; col<8; col++) {
            cout << jeu->grille[ligne][col]->couleur << " | ";
        }
        cout << "\n   +---+---+---+---+---+---+---+---+" << endl;
    }
}

void remplir_grille(Jeu *jeu) {
    for(int ligne=0; ligne<8; ligne++) {
        for(int col=0; col<8; col++) {
            //On alloque l'espace nécessaire pour placer chaque pointeur de jeton dans la grille.
            jeu->grille[ligne][col] = (Jeton*) malloc(sizeof(Jeton));
            
            jeu->grille[ligne][col]->couleur=' ';
            jeu->grille[ligne][col]->colonne = col;
            jeu->grille[ligne][col]->ligne = ligne;
        }
        
    }
    //On place les 4 premiers jetons d'une nouvelle partie.
    jeu->grille[3][3]->couleur=jeu->j1.couleur;
    jeu->grille[4][4]->couleur=jeu->j1.couleur;
    jeu->grille[3][4]->couleur=jeu->j2.couleur;
    jeu->grille[4][3]->couleur=jeu->j2.couleur;
    jeu->j1.nbJetons=2;
    jeu->j2.nbJetons=2;
}

//Pour faire une boucle while dans le main et continuer de jouer tant que la grille n'est pas pleine.
bool grille_incomplete(Jeu *jeu) {
    if ((jeu->j1.nbJetons+jeu->j2.nbJetons)!=64){
        return true;
    }
    else return false;
}