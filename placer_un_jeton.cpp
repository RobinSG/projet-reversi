/*placer_un_jeton.cpp*/
#include "placer_un_jeton.h"
#include <iostream>
using namespace std;

//Fonction qui vérifie qu'on est bien dans la grille.
bool dans_le_plateau(Jeu *jeu, int i, int j){
    return (i>=0 && i<8 && j>=0 && j<8);
}

//Fonction qui renvoie l'adversaire du joueur en paramètre.
Joueur *adversaire(Jeu *jeu, Joueur *joueurquiplace) {
    Joueur *autrejoueur;
    if (joueurquiplace==&jeu->j1) {
        autrejoueur = &jeu->j2;
    }
    else autrejoueur = &jeu->j1;
    return autrejoueur;
}
void cases_jouables(Jeu* jeu, Joueur* joueurquiplace);
int test_cases_jouables(Jeu* jeu, Joueur* joueurquiplace);

//Cette fonction renvoie un booléen: vrai si le jeton a bien été placé et faux si ce n'est pas le cas.
bool placer_un_jeton(Jeu *jeu, Joueur *joueurquiplace){
    bool reussi = false;
    char col; 
    int ligne;
    cout << "Tour de : " << joueurquiplace->nom;
    //Si aucun coup n'est possible, on renvoie faux.
    if (test_cases_jouables(jeu, joueurquiplace) == 0) {
        cout << ". Pas de coups possibles." << endl;
        return reussi;
    }
    else {
        cout << ". Position du jeton a placer (? pour voir les coups possibles): ";
        cin >> col;
        //Si on marque ?, on affiche tous les coups possibles.
        if (col == '?') {
            cases_jouables(jeu, joueurquiplace);
            cout << "\n";
            cin >> col >> ligne;
        }
        else {
            cin >> ligne;
        }
        //Verification des donnees entrees par l'utilisateur 
        if(ligne >= 9){
            cout << "La ligne doit être un chiffre compris entre 0 et 8. Recommencez. " << endl;
            return reussi;
        }
        int indicecol = (toupper(col)) - 'A';
        int indiceligne = ligne - 1;
        Joueur* autrejoueur = adversaire(jeu, joueurquiplace);


        bool result = false;

        //On utilise des boucles pour regarder dans les 8 directions autour de l'emplacement choisi pour vérifier qu'il y a bien un jeton adverse à côté.
        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                if (dans_le_plateau(jeu, indiceligne + i, indicecol + j) && jeu->grille[indiceligne + i][indicecol + j]->couleur == autrejoueur->couleur) {
                    int k = 2;
                    while (dans_le_plateau(jeu, indiceligne + k * i, indicecol + k * j)) {
                        if (jeu->grille[indiceligne + k * i][indicecol + k * j]->couleur == joueurquiplace->couleur) {
                            result = true;
                            break;
                        }
                        else if (jeu->grille[indiceligne + k * i][indicecol + k * j]->couleur == ' ') {
                            break;
                        }
                        k++;
                    }
                }
            }
        }

        if (result) {

            jeu->grille[indiceligne][indicecol]->couleur = joueurquiplace->couleur;
            jeu->grille[indiceligne][indicecol]->ligne = indiceligne;
            jeu->grille[indiceligne][indicecol]->colonne = indicecol;
            int jetons_captur = capturer_jetons(jeu, joueurquiplace, jeu->grille[indiceligne][indicecol]);
            //On ajoute le jeton placé lui-même puis les jetons capturés au nombre de jetons du joueur.
            joueurquiplace->nbJetons += 1;
            joueurquiplace->nbJetons += jetons_captur;
            //On retire les jetons capturés au nombre de jetons de l'adversaire.
            autrejoueur->nbJetons -= jetons_captur;
            //On annonce le nombre de jetons capturés.
            cout << "Vous avez capture " << jetons_captur << " jeton(s)! " << endl;
            reussi = true;
            return reussi;
        }
        cout << "Impossible de placer le jeton ici! " << endl;
        return reussi;
    }
}




//Fonction qui capture et renvoie le nombre de jetons capturés.
int capturer_jetons(Jeu *jeu, Joueur *joueurquiplace, Jeton *jeton_plac){
    int ligne_jeton, col_jeton;
    ligne_jeton=jeton_plac->ligne;
    col_jeton=jeton_plac->colonne;
    int nbJetons_captur=0;  

    for(int i=-1; i<=1; i++){    
        for(int j=-1; j<=1; j++){
            if(dans_le_plateau(jeu, ligne_jeton + i, col_jeton + j) && jeu->grille[ligne_jeton + i][col_jeton + j]->couleur!=jeton_plac->couleur && jeu->grille[ligne_jeton + i][col_jeton + j]->couleur != ' '){
                int k = 2;
                while(dans_le_plateau(jeu, ligne_jeton + k*i, col_jeton + k*j)){
                    if(jeu->grille[ligne_jeton + k*i][col_jeton + k*j]->couleur == jeton_plac->couleur){
                        --k;
                        for(; k>=1; --k){
                            jeu->grille[ligne_jeton + k*i][col_jeton + k*j]->couleur=jeton_plac->couleur;
                            nbJetons_captur++;
                
                        }
                        break;
                    }else if(jeu->grille[ligne_jeton + k*i][col_jeton + k*j]->couleur==' '){
                        break;
                    }
                    
                    k++;
                }
            }
        }
    }

    return nbJetons_captur;
}

int test_capturer_jetons(Jeu jeu, Joueur *joueurquiplace, Jeton jeton_plac);

//Fonction qui donne les cases jouables du joueur en paramètre.
void cases_jouables(Jeu *jeu, Joueur *joueurquiplace){
    Joueur *autrejoueur=adversaire(jeu, joueurquiplace);
    int nbr_cases = 0;
    cout << "Cases jouables: ";
    //On regarde chaque case de la grille pour trouver des jetons adverses.
    for(int ligne=0; ligne<8; ligne++){    
        for(int col=0; col<8; col++){
            if(jeu->grille[ligne][col]->couleur==' ') {
                bool case_valide = false;
                for(int i=-1; i<=1; i++){    
                    for(int j=-1; j<=1; j++){
                        if (dans_le_plateau(jeu, ligne + i, col + j) && jeu->grille[ligne+i][col+j]->couleur==autrejoueur->couleur) {
                            Jeton jeton_test;
                            jeton_test.couleur=joueurquiplace->couleur;
                            jeton_test.ligne=ligne;
                            jeton_test.colonne=col;
                            if (test_capturer_jetons(*jeu, joueurquiplace, jeton_test)>0) {
                                cout << (char)(col + 65) << ligne + 1 << " ";
                                case_valide = true;
                                nbr_cases += 1;
                                //On vérifie que la case a bien ete trouvee et affichee pour pouvoir utiliser le bool dans des fonctions pour sortir des deux for.
                            }
                        }
                        if (case_valide){
                            break;
                        }
                    }
                    if (case_valide){
                        break;
                    }
                }
            }
        }
    }
}

//Version avec comme paramètre jeu et pas son pointeur pour pas vraiment modifier la grille mais juste tester.
int test_capturer_jetons(Jeu jeu, Joueur *joueurquiplace, Jeton jeton_plac){
    int ligne_jeton, col_jeton;
    ligne_jeton=jeton_plac.ligne;
    col_jeton=jeton_plac.colonne;
    int nbJetons_captur=0;

    for(int i=-1; i<=1; i++){    
        for(int j=-1; j<=1; j++){
            if(dans_le_plateau(&jeu, ligne_jeton + i, col_jeton + j) && jeu.grille[ligne_jeton + i][col_jeton + j]->couleur!=jeton_plac.couleur && jeu.grille[ligne_jeton + i][col_jeton + j]->couleur != ' '){
                int k = 2;
                while(dans_le_plateau(&jeu, ligne_jeton + k*i, col_jeton + k*j)){
                    if(jeu.grille[ligne_jeton + k*i][col_jeton + k*j]->couleur == jeton_plac.couleur){
                        --k;
                        for(;k>=1;--k){
                           nbJetons_captur++;
                        }
                        break;
                    }else if(jeu.grille[ligne_jeton + k*i][col_jeton + k*j]->couleur==' '){
                        break;
                    }
                    
                    k++;
                }
            }
        }
    }
    return nbJetons_captur;
}

//Version qui renvoie un int pour vérifier la condition.
int test_cases_jouables(Jeu* jeu, Joueur* joueurquiplace) {
    Joueur* autrejoueur = adversaire(jeu, joueurquiplace);
    int nbr_cases = 0;
    //On regarde chaque case de la grille pour trouver des jetons adverses.
    for (int ligne = 0; ligne < 8; ligne++) {
        for (int col = 0; col < 8; col++) {
            if (jeu->grille[ligne][col]->couleur == ' ') {
                bool case_valide = false;
                for (int i = -1; i <= 1; i++) {
                    for (int j = -1; j <= 1; j++) {
                        if (dans_le_plateau(jeu, ligne + i, col + j) && jeu->grille[ligne + i][col + j]->couleur == autrejoueur->couleur) {
                            Jeton jeton_test;
                            jeton_test.couleur = joueurquiplace->couleur;
                            jeton_test.ligne = ligne;
                            jeton_test.colonne = col;
                            if (test_capturer_jetons(*jeu, joueurquiplace, jeton_test) > 0) {
                                case_valide = true;
                                nbr_cases += 1;
                                //On verifie que la case a bien ete trouvee et affichee pour pouvoir utiliser le bool dans des fonctions pour sortir des deux for.
                            }
                        }
                        if (case_valide) {
                            break;
                        }
                    }
                    if (case_valide) {
                        break;
                    }
                }
            }
        }
    }
    return nbr_cases;
}
