/*placer_un_jeton.h*/
#ifndef PLACER_JETON
#define PLACER_JETON

#include "struct.h"

bool placer_un_jeton(Jeu *jeu, Joueur *joueurquiplace);

int capturer_jetons(Jeu *jeu, Joueur *joueurquiplace, Jeton *jeton_plac);

Joueur* adversaire(Jeu* jeu, Joueur* joueurquiplace);

int test_cases_jouables(Jeu* jeu, Joueur* joueurquiplace);

#endif