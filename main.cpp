#include <iostream>
#include <string.h>
#include "struct.h"
#include "fonction_grille.h"
#include "placer_un_jeton.h"
#include "fonction_Joueur.h"
using namespace std;

int main() {
    Jeu jeu;
    cout << "Joueur 1: ";
    lireJoueur(&jeu.j1);
    cout << "Joueur 2: ";
    lireJoueur(&jeu.j2);
    //On vérifie que les jetons choisis soient bien différents.
    if (jeu.j2.couleur == jeu.j1.couleur){
        cout << "Vous ne pouvez pas avoir le meme jeton que " << jeu.j1.nom << ", c'est a dire '" << jeu.j1.couleur << "'. Reessayez." << endl;
        lireJoueur(&jeu.j2);
    }
    //On remplit la grille pour commencer le jeu.
    remplir_grille(&jeu);
    //On affiche le nombre de jetons de chaque joueur.
    ecrireJoueur(jeu.j1);
    ecrireJoueur(jeu.j2);
    affiche_grille(&jeu);

    //On initialise avec j1 qui commence.
    Joueur* joueur_qui_commence = &jeu.j1;
    //On reste dans la boucle tant que la grille n'est pas pleine et qu'au moins un des joueurs peut jouer.
    while (grille_incomplete(&jeu) && (test_cases_jouables(&jeu, joueur_qui_commence) > 0 || test_cases_jouables(&jeu, adversaire(&jeu, joueur_qui_commence)) > 0)) {
        //Le joueur recommence tant qu'il n'a pas fait un coup valable.
        while (placer_un_jeton(&jeu, joueur_qui_commence)) {
            affiche_grille(&jeu);
            ecrireJoueur(*joueur_qui_commence);
            joueur_qui_commence = adversaire(&jeu, joueur_qui_commence);
            ecrireJoueur(*joueur_qui_commence);
        }
        //Si le joueur n'a pas de coup possible, c'est au tour de l'adversaire de jouer.
        if (test_cases_jouables(&jeu, joueur_qui_commence) == 0) {
            joueur_qui_commence = adversaire(&jeu, joueur_qui_commence);
        }
    }
    if (jeu.j1.nbJetons > jeu.j2.nbJetons) {
        cout << jeu.j1.nom << " remporte la partie avec " << jeu.j1.nbJetons << " jetons! Bravo! " << endl;
    }
    else if (jeu.j1.nbJetons == jeu.j2.nbJetons) {
        cout << "Incroyable! C'est une égalité parfaite avec " << jeu.j1.nbJetons << " jetons chacun!" << endl;
    }
    else cout << jeu.j2.nom << " remporte la partie avec " << jeu.j2.nbJetons << " jetons! Bravo! " << endl;
    return 0;
}