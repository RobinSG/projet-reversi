/*fonction_Joueur.cpp*/
#include <iostream>
#include <string.h>
#include "fonction_Joueur.h"
using namespace std;

void lireJoueur(Joueur *joueur) {
    char name[20];
    char couleur;
    cout << "Entrez votre nom: ";
    cin >> name;
    strcpy(joueur->nom, name);
    cout << "Choisissez votre symbole de pion: ";
    cin >> couleur;
    joueur->couleur=couleur;
    joueur->nbJetons=0;
}

void ecrireJoueur(Joueur joueur) {
    cout << joueur.nom << " possede " << joueur.nbJetons;
    if (joueur.nbJetons<=1) {
        cout << " jeton (" << joueur.couleur << ")";
    }
    else {
        cout << " jetons ("<< joueur.couleur << ")";
    }
    cout << "." << endl;
}