# MAKEFILE PROGRAMME REVERSI 

reversi: fonction_grille.o placer_un_jeton.o fonction_Joueur.o main.o
	g++ -o reversi fonction_grille.o placer_un_jeton.o fonction_Joueur.o main.o -Wall -lstdc++ -g
	
fonction_grille.o: fonction_grille.cpp fonction_grille.h struct.h
	g++ -c fonction_grille.cpp -Wall -g

placer_un_jeton.o: placer_un_jeton.cpp placer_un_jeton.h struct.h
	g++ -c placer_un_jeton.cpp -Wall -g

fonction_Joueur.o: fonction_Joueur.cpp fonction_Joueur.h struct.h
	g++ -c fonction_Joueur.cpp -Wall -g
    
main.o: main.cpp struct.h fonction_grille.h placer_un_jeton.h fonction_Joueur.h
	g++ -c main.cpp -Wall -g
	
clean:
	rm *.o
	rm reversi
	
	
