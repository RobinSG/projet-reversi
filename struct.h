/* struct.h */
#ifndef STRUCT
#define STRUCT

typedef struct Jeton{
    char couleur;
    int ligne;
    int colonne;
}   Jeton;


typedef struct Joueur{
    char nom[20];
    char couleur;
    int nbJetons;
}   Joueur;

typedef struct Jeu{
    Joueur j1;
    Joueur j2;
    Jeton* grille[8][8];
}   Jeu;

#endif