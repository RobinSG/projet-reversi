/*fonction_grille.h*/
#ifndef FONCTION_GRILLE
#define FONCTION_GRILLE

#include "struct.h"

void affiche_grille(Jeu *jeu);

void remplir_grille(Jeu *jeu);

bool grille_incomplete(Jeu* jeu);

#endif
